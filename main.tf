terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "tp5-rg" {
  name     = "tp5-rg"
  location = "eastus"
}


# Public IP Node 1
resource "azurerm_public_ip" "tp5-ip" {
  name                = "public-ip-tp5"
  resource_group_name = azurerm_resource_group.tp5-rg.name
  location            = azurerm_resource_group.tp5-rg.location
  allocation_method   = "Dynamic"
}

resource "azurerm_virtual_network" "tp5-vn" {
  name                = "tp5-vn-vm"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.tp5-rg.location
  resource_group_name = azurerm_resource_group.tp5-rg.name
}

resource "azurerm_subnet" "tp5-sub" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.tp5-rg.name
  virtual_network_name = azurerm_virtual_network.tp5-vn.name
  address_prefixes     = ["10.0.2.0/24"]
}

# NIC 1
resource "azurerm_network_interface" "tp5-nic1" {
  name                = "tp5-nic1"
  location            = azurerm_resource_group.tp5-rg.location
  resource_group_name = azurerm_resource_group.tp5-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.tp5-sub.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.tp5-ip.id
  }
}

# Create VM TP5
resource "azurerm_linux_virtual_machine" "tp5-rg" {
  name                            = "tp5-nclone"
  resource_group_name             = azurerm_resource_group.tp5-rg.name
  location                        = azurerm_resource_group.tp5-rg.location
  size                            = "Standard_B1s"
  admin_username                  = "cicd"
  disable_password_authentication = true
  network_interface_ids = [
    azurerm_network_interface.tp5-nic1.id,
  ]

  admin_ssh_key {
    username   = "cicd"
    public_key = file("./id-rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
}
